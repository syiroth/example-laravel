<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

// MVC= MODEl, VIEW (route) controller

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });

Route::get('dashboard', [DashboardController::class, 'index']);

Route::get('articles', [ArticleController::class, 'index']);
Route::get('articles/create', [ArticleController::class, 'create']);

// Route::get('/halaman-satu', function () {
//     return view('page-satu');
// });

// Route::get('/halaman-dua', function () {
//     return view('page-dua');
// });