<!DOCTYPE html>
<html lang="en">

    
<head>
        <meta charset="utf-8" />
        <title>Miftakhusy Syiroth</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <link rel="shortcut icon" href="{{ asset('template/images/favicon.ico') }}">
        <!-- App css -->
        <link href="{{ asset('template') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="{{ asset('template') }}/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('template') }}/css/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            
            <!-- Topbar Start -->
            @include('layouts.navbar')
            <!-- end Topbar --> 
            <!-- ========== Left Sidebar Start ========== -->

            @include('layouts.sidebar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start container-fluid -->
                    <div class="container-fluid">

                        {{-- container --}}

                        @yield('content')

                        {{-- container --}}

                    </div>
                    <!-- end container-fluid -->

                    

                    <!-- Footer Start -->
                    @include('layouts.footer')
                    <!-- end Footer -->

                </div>
                <!-- end content -->

            </div>
            <!-- END content-page -->

        </div>
        <!-- END wrapper -->

        <!-- Right bar overlay-->
        {{-- <div class="rightbar-overlay"></div> --}}

        <a href="javascript:void(0);" class="right-bar-toggle demos-show-btn">
            <i class="mdi mdi-settings-outline mdi-spin"></i> &nbsp;Choose Demos
        </a>

        <!-- Vendor js -->
        <script src="{{ asset('template') }}/js/vendor.min.js"></script>

        <script src="{{ asset('template') }}/libs/morris-js/morris.min.js"></script>
        <script src="{{ asset('template') }}/libs/raphael/raphael.min.js"></script>

        <script src="{{ asset('template') }}/js/pages/dashboard.init.js"></script>

        <!-- App js -->
        <script src="{{ asset('template') }}/js/app.min.js"></script>

    </body>

</html>